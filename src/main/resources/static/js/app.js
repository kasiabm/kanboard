(function() {
"use strict";

angular.module("kanboard", [])
.controller("KanboardController", KanboardController)
.service("KanboardService", KanboardService)
.directive("board", KanboardDirective);

KanboardController.$inject = ["KanboardService"];
function KanboardController(KanboardService) {
    var kanCtrl = this;
    kanCtrl.boardCreated = false;

    kanCtrl.addDefaultBoard = function() {
        kanCtrl.board = KanboardService.defaultBoard();
        kanCtrl.boardCreated = true;
    }

    kanCtrl.addCustomBoard = function(boardTitle, noOfColumns) {
        KanboardService.customBoard(boardTitle, noOfColumns);
        kanCtrl.boardCreated = true;
    }
}

KanboardService.$inject = ["$http"];
function KanboardService($http) {
    var kanService = this;
    var board = {
        "title" : "My board",
        "columns" : [
            {
                "columnName" : "To Do",
                "maxItems" : 5
            },
            {
                "columnName" : "Doing",
                "maxItems" : 5
            },
            {
                "columnName" : "Done",
                "maxItems" : 5
            }
        ]
    };
    kanService.defaultBoard = function() {
        $http.put("/defaultBoard", board);
        return board;
    }

    kanService.customBoard = function (boardTitle, noOfColumns) {
        board = {
            "board": {
                "name": boardTitle,
                "noOfColumns": noOfColumns
            }
        };
        $http.put("/customBoard", board);
    }
}

function KanboardDirective() {
	return {
		templateUrl: "/templates/board.html"
	};
}
})();
