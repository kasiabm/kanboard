package com.kasiamacrae.kanboard.web;

import com.kasiamacrae.kanboard.domain.Board;
import com.kasiamacrae.kanboard.domain.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
public class BoardController {

    @Autowired
    private BoardRepository boardRepository;

    @RequestMapping("/")
    public String index() {
        return "index.html";
    }

    @RequestMapping("/defaultBoard")
    public @ResponseBody void newBoard() {
        boardRepository.save(new Board());
    }

    @RequestMapping("/customBoard")
    public @ResponseBody void customBoard(final @RequestBody HashMap<String, HashMap<String, String>> requestData) {
        final HashMap<String, String> boardData = requestData.get("board");
        final String name = boardData.get("name");
        final String noOfColumns = boardData.get("noOfColumns");
        boardRepository.save(new Board(name, noOfColumns));
    }
}
