package com.kasiamacrae.kanboard.domain;

import java.util.ArrayList;

public class Column {

    private Title title;
    private MaxCards maxCards;
    private ArrayList<Card> cards;

    public Column() {
        title = new Title("New Column");
        maxCards = new MaxCards();
        cards = new ArrayList<>();
    }

    public Column(final String title, final String maxCards) {
        this.title = new Title(title);
        this.maxCards = new MaxCards(maxCards);
        cards = new ArrayList<>();
    }

    public String getTitle() {
        return title.getTitle();
    }

    public String getMaxCards() {
        return maxCards.getMaxCards();
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void rename(final String newTitle) {
        title = new Title(newTitle);
    }

    public void updateMaxCards(final String newMaxCards) {
        maxCards = new MaxCards(newMaxCards);
    }

    public void addCard(final Card card) {
        cards.add(card);
    }

    public void removeCard(final Card card) {
        if (cards.contains(card)) {
            cards.remove(card);
        }
    }
}
