package com.kasiamacrae.kanboard.domain;

public class MaxCards {

    private final int maxCards;

    public MaxCards() {
        maxCards = 5;
    }

    public MaxCards(final String maxCards) throws IllegalArgumentException {
        validateMaxCards(maxCards);
        this.maxCards = toInt(maxCards);
    }

    private void validateMaxCards(final String maxCards) {
        if (toInt(maxCards) <= 0) {
            throw new IllegalArgumentException("Max cards value must be at least 1.");
        }
    }

    private int toInt(final String maxCards) {
        return Integer.parseInt(maxCards);
    }

    public String getMaxCards() {
        return String.valueOf(maxCards);
    }
}
