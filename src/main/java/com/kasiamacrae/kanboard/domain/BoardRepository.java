package com.kasiamacrae.kanboard.domain;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface BoardRepository extends MongoRepository<Board, String> {

    Board findByTitle(final Title title);
}
