package com.kasiamacrae.kanboard.domain;

public class Card {

    private Description description;

    public Card() {
        description = new Description();
    }

    public Card(final String description) {
        this.description = new Description(description);
    }

    public String getDescription() {
        return description.getDescription();
    }

    public void updateDescription(final String newDescription) {
        description = new Description(newDescription);
    }
}
