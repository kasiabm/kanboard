package com.kasiamacrae.kanboard.domain;

public class Title {

    private final String title;

    public Title(final String title) throws IllegalArgumentException {
        validateTitle(title);
        this.title = title;
    }

    private void validateTitle(final String title) {
        if (title == null || title.isEmpty()) {
            throw new IllegalArgumentException("Title must be provided.");
        }
    }

    public String getTitle() {
        return title;
    }
}
