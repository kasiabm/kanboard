package com.kasiamacrae.kanboard.domain;

public class Description {

    private final String description;

    public Description() {
        description = "Description here";
    }

    public Description(final String description) throws IllegalArgumentException {
        validateDescription(description);
        this.description = description;
    }

    private void validateDescription(final String description) {
        if (description == null || description.isEmpty()) {
            throw new IllegalArgumentException("Description must be provided.");
        }
    }

    public String getDescription() {
        return description;
    }
}
