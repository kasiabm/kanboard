package com.kasiamacrae.kanboard.domain;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;

public class Board {

    @Id
    private String id;
    private Title title;
    private ArrayList<Column> columns;

    public Board() {
        setDefaultBoard();
    }

    private void setDefaultBoard() {
        final String[] defaultColumnTitles = {"To do", "Doing", "Done"};
        final String defaultMaxCards = "5";
        title = new Title("New Board");
        columns = new ArrayList<>();
        for (final String columnTitle : defaultColumnTitles) {
            columns.add(new Column(columnTitle, defaultMaxCards));
        }
    }

    public Board(final String title, final String noOfColumns) {
        this.title = new Title(title);
        setColumns(noOfColumns);
    }

    private void setColumns(final String noOfColumns) {
        final int columnsCount = Integer.parseInt(noOfColumns);
        columns = new ArrayList<>();
        for (int i = 0; i < columnsCount; i++) {
            columns.add(new Column("Column " + (i + 1), "5"));
        }
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title.getTitle();
    }

    public ArrayList<Column> getColumns() {
        return columns;
    }

    public void rename(final String newTitle) {
        title = new Title(newTitle);
    }

    public void addColumn(final Column column) {
        columns.add(column);
    }

    public void removeColumn(final Column column) {
        if (columns.contains(column)) {
            columns.remove(column);
        }
    }
}
