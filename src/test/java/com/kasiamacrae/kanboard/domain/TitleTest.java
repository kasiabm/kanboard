package com.kasiamacrae.kanboard.domain;

import org.junit.Test;

public class TitleTest {

    @Test(expected = IllegalArgumentException.class)
    public void new_shouldThrowInvalidArgumentExceptionForInvalidTitleValue() {
        new Title(null);
    }
}
