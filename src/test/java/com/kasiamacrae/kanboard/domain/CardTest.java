package com.kasiamacrae.kanboard.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CardTest {

    @Test
    public void updateDescription_shouldUpdateDescription() {
        final Card card = new Card();
        final String newDescription = "Test web interface";

        card.updateDescription(newDescription);

        assertEquals(newDescription, card.getDescription());
    }
}
