package com.kasiamacrae.kanboard.domain;

import org.junit.Test;

public class MaxCardsTest {

    @Test(expected = IllegalArgumentException.class)
    public void new_shouldThrowInvalidArgumentExceptionForInvalidMaxCardsValue() {
        new MaxCards("0");
    }
}
