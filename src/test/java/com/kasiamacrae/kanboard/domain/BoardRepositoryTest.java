package com.kasiamacrae.kanboard.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BoardRepositoryTest {

    @Autowired
    private BoardRepository boardRepository;
    private Board board;

    @Before
    public void before() {
        boardRepository.deleteAll();
    }

    @Test
    public void save_shouldSaveBoardWithDefaultSettings() {
        board = boardRepository.save(new Board());

        assertNotNull(board.getId());
    }

    @Test
    public void save_shouldSaveBoardWithGivenSettings() {
        board = boardRepository.save(new Board("My Board", "5"));

        assertNotNull(board.getId());
    }

    @Test
    public void findByName_shouldFindBoardByName() {
        boardRepository.save(new Board());

        board = boardRepository.findByTitle(new Title("New Board"));

        assertEquals("New Board", board.getTitle());
        assertEquals(3, board.getColumns().size());
        assertEquals("To do", board.getColumns().get(0).getTitle());
        assertEquals("5", board.getColumns().get(0).getMaxCards());
    }
}
