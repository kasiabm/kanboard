package com.kasiamacrae.kanboard.domain;

import org.junit.Test;

public class DescriptionTest {

    @Test(expected = IllegalArgumentException.class)
    public void new_shouldThrowInvalidArgumentExceptionForInvalidDescriptionValue() {
        new Description(null);
    }
}
