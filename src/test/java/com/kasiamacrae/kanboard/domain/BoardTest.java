package com.kasiamacrae.kanboard.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BoardTest {

    private String title;
    private Board board;

    @Before
    public void before() {
        title = "My board";
        board = new Board();
    }

    @Test
    public void new_shouldCreateNewBoardWithDefaultSettings() {
        final String[] columnTitles = {"To do", "Doing", "Done"};
        final String maxItems = "5";

        assertEquals(3, board.getColumns().size());
        int index = 0;
        for (final Column column : board.getColumns()) {
            assertEquals(columnTitles[index], column.getTitle());
            assertEquals(maxItems, column.getMaxCards());
            index++;
        }
    }

    @Test
    public void new_shouldCreateNewBoardWithGivenSettings() {
        board = new Board(title, "4");

        assertEquals(title, board.getTitle());
        assertEquals(4, board.getColumns().size());
    }

    @Test
    public void rename_shouldRenameBoard() {
        board.rename("New Title");

        assertEquals("New Title", board.getTitle());
    }

    @Test
    public void addColumn_shouldAddNewColumn() {
        final Column column = new Column();

        board.addColumn(column);

        assertEquals(4, board.getColumns().size());
        assertTrue(board.getColumns().contains(column));
    }

    @Test
    public void removeColumn_shouldRemoveColumn() {
        board.removeColumn(board.getColumns().get(2));

        assertEquals(2, board.getColumns().size());
    }
}
