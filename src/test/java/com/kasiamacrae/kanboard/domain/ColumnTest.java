package com.kasiamacrae.kanboard.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ColumnTest {

    private Column column;
    private String cardDescription;
    private Card card;

    @Before
    public void before() {
        column = new Column();
        cardDescription = "Test GUI";
        card = new Card(cardDescription);
    }

    @Test
    public void new_shouldCreateNewColumnWithValidAttributesValues() {
        assertEquals("New Column", column.getTitle());
        assertEquals("5", column.getMaxCards());
    }

    @Test
    public void rename_shouldRenameColumn() {
        column.rename("New name");

        assertEquals("New name", column.getTitle());
    }

    @Test
    public void updateMaxCards_shouldUpdateMaxCards() {
        column.updateMaxCards("8");

        assertEquals("8", column.getMaxCards());
    }

    @Test
    public void addCard_shouldAddNewCard() {
        column.addCard(card);

        assertEquals(1, column.getCards().size());
        assertTrue(column.getCards().contains(card));
    }


    @Test
    public void removeCard_shouldRemoveCard() {
        column.addCard(card);

        column.removeCard(card);

        assertEquals(0, column.getCards().size());
        assertFalse(column.getCards().contains(card));
    }
}
